const { Router } = require('express')
const multer = require('multer')
const fs = require('fs')
const { exec } = require('child_process')
const abc = require('./alphabet')

// const amRecoDir = '/home/dato/Documents/magister/code/geo_symbol_recognition/recognition/am/images/16'
// const nsRecoDir = '/home/dato/Documents/magister/code/geo_symbol_recognition/recognition/ns/images/16'
const uploadsDir = '/home/dato/Documents/magister/code/uploads'

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, uploadsDir)
  },
  filename: function (req, file, cb) {
    req.imageName = file.fieldname + '-' + Date.now() + '.png'
    cb(null, req.imageName)
  }
})
 
const upload = multer({ storage: storage })

const router = new Router()

router.get('/test', (req, res) => {
  res.send('OK')
})


const recoInputAM = '/home/dato/Documents/magister/code/geo_symbol_recognition/reco_input_am.json'
const recoInputNS = '/home/dato/Documents/magister/code/geo_symbol_recognition/reco_input_ns.json'
const pythonCommand = `python3 /home/dato/Documents/magister/code/geo_symbol_recognition/main.py -r`
const refuseText = 'ამოცნობაზე უარი ითქვა'

router.post('/', upload.single('img'), (req, res) => {
  let recoInputFile = ''
  let langFlag = ''
  if (req.body.lang == 'am') {
    recoInputFile = recoInputAM
    langFlag = '-a'
  } else {
    recoInputFile = recoInputNS
    langFlag = '-n'
  }

  const rawData = fs.readFileSync(recoInputFile)
  const inputJson = JSON.parse(rawData)
  inputJson.img_path = uploadsDir + '/' + req.imageName
  fs.writeFileSync(recoInputFile, JSON.stringify(inputJson, null, 2))

  exec(`${pythonCommand} ${langFlag}`, (err, stdout, stderr) => {
    console.log(req.file)
    if (err) {
      console.log(err)
      console.log('cannot execute !!!')
      res.status(400).json({image: req.file.originalname, text: refuseText})
    } else if (stderr) {
      console.log(stderr)
      res.status(401).json({image: req.file.originalname, text: refuseText})
    } else {
      const result = processOut(stdout, req.body.count, req.body.lang)
      res.json(Object.values(result))
    }
  })
})

module.exports = router


function processOut(str, count, lang) {
  const res = str.substring(2, str.length - 2)
                .split('), (')
                .map(e => e.split(', '))
                .map(e => [e[0].substring(1, e[0].length - 1), Number(e[1])])
	const out = {}
	for (r of res) {
    if (Object.keys(out).length == count) {
      break
    }
    const prefix = getPrefix(r[0])
		if (!(prefix in out)) {
      out[prefix] = {
        img: `http://localhost:3000/${lang}/${prefix}.png`,
        name: `${abc[prefix]}`,
        percent: r[1]
      }
    }
	}
	return out
}

function getPrefix(imgName) {
	return imgName.substring(0, imgName.indexOf('_'))
}